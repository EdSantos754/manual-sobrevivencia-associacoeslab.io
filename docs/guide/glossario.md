# Glossário

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::

Ao percorrer este guia, poderá nao estar familizaridada(o) com todos
os termos. Caso encontre algum cuja definição não esteja aqui,
sinta-se à vontade de pedir que o adicionemos, criando uma conta
gitlab e submetendo [este
formulário](https://gitlab.com/manual-sobrevivencia-associacoes/manual-sobrevivencia-associacoes.gitlab.io/issues/new).

## órgãos sociais

TODO

## A associação obriga-se a X

Esta é uma expressão que deverá estar nos estatutos e significa que 


## Órgãos estatutários

Tipicamente:
  - Direção
  - Conselho Fiscal
  - Mesa da Assembleia Geral
  - (opcional) outros que estejam definidos nos estaututos
  
TODO: isto é equivalente a [Membros de órgãos
estatutários](#membros-de-orgaos-estatutarios)?


## Membros de órgãos estatutários

Também conhecido como MOEs, é o nome que a Segurança Social dá aos
membros da direção (confirmar que os outros órgãos sociais também não
contam). 

Mais informação no [site da Segurança Social](http://www.seg-social.pt/membros-de-orgaos-estatutarios).


## Mesa da Assembleia

Órgão social responsável por moderar as assembleia geral. Não é
necessário nomear-se membros para isto nas eleições (excepto se os
estatutos disserem algo em contrário) pois quando há uma AG, pode no
início nomear-se as pessoas que vão desempenhar esse papel durante a
AG.

## Técnico Oficial de Contas

Também conhecido como TOC. TODO descrever


## Bus factor

Quantas pessoas é que precisam de ser atropeladas por um autocarro até
ja a associação morrer. (ver na [wikipédia](https://en.wikipedia.org/wiki/Bus_factor))


## Suprimentos

"capitalização" da associação (injeção de fundos numa fase inicial) em
que esta se obriga a devolver. Pode ser capital, mas também materiais
(ex: computador, mesas, etc.). Tem de ser registado na contabilidade e
convem ser o mais específico possível (numero de série, modelo, etc.)


## Fundo de Maneio

(dinheiro de caixa) - algum dinheiro vivo (~100-200€) presente para
fazer pequenos pagamentos (canetas, stickers, etc.). À sua gestão
chama-se *gestão de caixa*.

## RNPC

Registo Nacional de Pessoas Coletivas. Website diponível
[aqui](https://www.irn.mj.pt/sections/irn/a_registral/rnpc).


