# Livro de Atas

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::

[**livro de atas**](https://www.fnac.pt/Livro-de-Actas-Varios/a349804)
com os membros eleitos para os órgãos sociais.

> **A que formalidades deve obedecer o livro de atas?**
>
>R: Os livros de atas podem ser constituídos por folhas soltas
>numeradas sequencialmente e rubricadas pela Administração ou pelos
>membros do órgão social a que respeitam ou, quando existam, pelo
>secretário da sociedade ou pelo Presidente da Mesa da Assembleia
>Geral da sociedade, que lavram, igualmente, os termos de abertura e
>de encerramento, devendo as folhas soltas ser encadernadas depois de
>utilizadas. --
>[fonte](http://www.empresanahora.pt/ENH/sections/PT_faq/)

Elementos necessários do livro de atas:

| Elemento          | Descrição                                                                         |
|-------------------|-----------------------------------------------------------------------------------|
| Cabeçalho         | Nome da associação, NIPC, TODO outras coisas                                      |
| Folhas numeradas  | Numeração para que não seja possível "desaparecer" uma folha                      |
| Termo de abertura | No início de cada livro de atas (TODO adicionar exemplo no apêndice)              |
| Termo de fecho    | No fim de cada livro de atas (TODO adicionar exemplo no apêndice                  |
| Rúbrica           | Do presidente da mesa da AG (ou do órgão que se está a reunir) em todas as folhas |
| Assinatura        | Dos membros da mesa da assembleia                                                 |

