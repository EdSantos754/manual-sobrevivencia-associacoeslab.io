# Criar a Associação

Muito bem, entendidos os custos e confiando uns nos outros, estão
então prontos para partir para a aventura de criar uma associação.

Há duas formas de o fazer:
  - **[Associação na Hora](#via-associacao-na-hora)** - processo
    mais simples mas com menos flexibilidade. Custa 300€
  - **[Processo tradicional](#via-processo-tradicional)** - mais
    demorado mas que permite definir estatutos e órgãos logo de
    início. Custo variável.

Para quem tenha menos recursos, o processo recomendado é o da
Associação na Hora. Basta dirigir-se a um balcão com a papelada toda
em três horas (ao contrario do que o nome indica!) terão a associação
a existir legalmente (mas as finanças e a segurança social ainda não
sabem disso!).

## via Associação na Hora

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::

> [A Lei n.º 40/2007, de 24 de Agosto][lei 40/2007], aprovou o regime
> especial de constituição imediata de associações e actualizou o
> regime geral de constituição previsto no Código Civil. Com o serviço
> Associação na Hora passa a ser possível constituir uma associação
> num único balcão e de forma imediata. [fonte][1]
>
> **Visão Geral**O processo de criação de uma associação na hora é
> feita através da deslocação de dois membros fundadores da associação
> a um balcão de associação na hora e nesse mesmo dia fica concluida a
> sua criação

### Pressupostos
*(de acordo com a [A Lei n.º 40/2007, de 24 de
Agosto](https://dre.pt/pesquisa/-/search/640908/details/maximized))*
  - nome escolhido [desta
    list](http://www.associacaonahora.mj.pt/seccoes/denominacoes.htm)
    ou pre-aprovado (certificado de admissibilidade de denominação)
 - Não é nenhum
   [destes](http://www.associacaonahora.mj.pt/seccoes/ANH-Faqs.pdf#page6)
   seguintes tipos de associação

### Escolher o nome
Há 3 maneiras de escolher o nome: ([fonte][FAQ Associação na Hora])
 - nome escolhido [desta
   list](http://www.associacaonahora.mj.pt/seccoes/denominacoes.htm)
 - pre-aprovado ([certificado de admissibilidade de
   denominação](https://www.irn.mj.pt/IRN/sections/irn/a_registral/rnpc/docs_rnpc/novos-impressos-do-rnpc/downloadFile/attachedFile_f0/RNPC_Mod1.pdf))
 - nos postos de atendimento (???)

Se escolhermos um nome pré-aprovado, podemos depois adicinar um
subnome tipo "privacylx de cidadãos" ([pergunta
12](http://www.associacaonahora.mj.pt/seccoes/ANH-Faqs.pdf#page6)) ou
al como descrito no ponto 1 do artigo 9º da [lei
40/2007](https://dre.pt/pesquisa/-/search/640908/details/maximized).

Segundo o ponto 3 do artigo 9º da [lei
40/2007](https://dre.pt/pesquisa/-/search/640908/details/maximized)
parece ser obrigatório o nome ser algo como "**associação** [nome]"

### Tarefas antes de se dirigir ao balcão

#### Escolher Estatutos

Criar uma associação por esta via tem a desvantagem de não permitir
definir os próprios estatutos. Em alternativa, é necessário escolher
os estatutos a partir de [uns modelos pré-aprovados](http://www.associacaonahora.mj.pt/seccoes/estatutos.htm).

O modelo vai conter espaços em branco que é necessário preencher.

::: warning AVISO

Um dos campos por preencher será a missão da associação. Deve prestar
particular detalhe a este campo pois ele define o âmbito das
atividades da associação, ou seja, toda e qualquer coisa que a sua
associação fará tem que estar inserida nesse contexto, mas também não
pode ser demasiado âmplo.

:::

##### Sede da Associação
É o local para onde será enviada correspondência

> A sede também deve ser um dos elementos a definir, uma vez que tem
> de estar fixada no texto dos estatutos. Normalmente, a sede
> refere-se ao local onde funciona a administração principal da
> associação, mas pode ser escolhida outra morada.

Q: Esta morada pode ser um apartado?

#### Escolher tipo de contabilidade

Pode ser mais sobre a diferênça entre os dois tipos de contabilidade
[aqui](contabilidade.md).

::: warning AVISO

Dependendo do volume de transações e pagamento ou não de funcionários
poderá ser obrigatório ter contabilidade organizada. Por favor
confirme.

:::

Caso opte por [contabilidade
organizada](contabilidade.md#contabilidade-organizada) terá de
designar um [Técnico(a) Oficial de Contas
(TOC)](glossario.md#tecnico-oficial-de-contas) com o(a) qual já tenha
contacto ou poderá escolher um(a) da bolsa de TOC's disponibilizada no
balcão. Isto tem um custo de 50€+IVA ([FAQ pergunta 22][FAQ Associação na
Hora]) - escolha do contablista certificado da bolsa


### No balcão assoicação na hora (ANH)

Deve neste momento devem dirigir-se a uma balcão [Associação na
Hora](https://irn.justica.gov.pt/Balcoes-Associacao-na-Hora):
  - **duas pessoas** - caso tenham escolhido um modelo de estatutos
    sem nomeação
  - **nove pessoas** que serão nomeadas para os [órgãos
    sociais](glossario.md#orgaos-estatutarios) caso tenham escolhido
    um modelo de estatutos com nomeação

#### Documentos a entregar
- documento de indentificação (C.C. / B.I. / Passaporte / Autorização
  de Residência)
- cartão de contribuinte
- fornecer assinatura > **Nota:** As pessoas singulares estrangeiras
que não sejam membros de nenhum órgão social ficam dispensadas da
apresentação do número de identificação fiscal.

#### Preencher ato constitutivo
Será um documento semelhante a este:
>![folha-criacao-associacao|363x500](upload://kqJyFHNdZjuxFdLzZwrgixlo4im.jpeg)

#### Preencher os Estatutos
Baseado no modelo pré-aprovado

#### Pagar
Pagar os 300€ do ato constitutivo de uma associação

#### (opcional) declarar logo início de atividade

Podemos dar logo estes dados (citation needed)

::: tip NOTA

Num caso, não foi pedido isto, pelo que a associação teve de o fazer
manualmente no portal das finanças

:::


#### Documentos a Receber
No ato de constituição, são nos dados os sequintes documentos (sem
custos adicionais): (Artigo 11º da [lei
40/2007](https://dre.pt/pesquisa/-/search/640908/details/maximized))
- certidão do ato constitutivo e dos estatutos
- recibo do pagamento
- cartão eletrónico de pessoa coletiva (+ código de acesso)
- cartão físico de pessoa coletiva

::: tip DICA

Tomar também nota do **NIPC** (número de identificação de pessoa
coletiva) e o **CAE** (código de atividade) pois podem ser
necessários para a declaração de início de atividade.

:::

### O que fazer a seguir?
Neste momento o ato de constituição da associação deverá ser publicado
em [neste site](www.mj.gov.pt/publicações) e a associação existirá
legalmente, mas ainda é necessário mais alguns passos até o processo
de criação estar completamente concluído.

#### Organizar a primeira assembleia geral

Ler sobre como o fazer [aqui](assembleias-gerais.md#primeira-assembleia-geral).
### Outras tarefas

Prossiga para ["Após criar Associação"](apos-criar-associacao.md) para
ler os próximos passos.

---------------------------------------------------------------------------------

### Recursos
* [FAQ Associação na Hora][FAQ Associação na Hora]

## via Processo Tradicional

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::

Este processo é uma alternativa à criação através da [associação na
hora](#via-associacao-na-hora).

O processo está documentado aqui [aqui][1].

[1]: https://web.archive.org/web/20181013182403/https://cdp.portodigital.pt/empreendedorismo/como-criar-uma-associacao/passoas-para-a-criacao-de-uma-associacao/
