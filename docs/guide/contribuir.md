# Ajudar a melhorar este manual 

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::

O manual de sobrevivência das Associações em Portugal é feito por
todos nós e precisa de todos o ajudem para que ele possa ajudar a
próxima geração da economia social.

Se achar que tem algo a acrescentar ou corrigir, essa ajuda será muito
apreciada!

## Em que é que posso ajudar?

- **Dar uma vista de olhos [tarefas por
  fazer](https://gitlab.com/manual-sobrevivencia-ongs/manual-sobrevivencia-ongs.gitlab.io/issues).**
  Pode ver o que precisa de ajuda, dar seu comentário e propor uma
  mudança.
- **Propor a adição de novo conteúdo.** O mundo está em constante
  mudança e por isso há sempre novas coisas a adicionar. A maneira
  mais fácil é criar uma conta gitlab (onde o projeto está alojado)
  e preencher [este formulário](https://gitlab.com/manual-sobrevivencia-ongs/manual-sobrevivencia-ongs.gitlab.io/issues/new)
- **Melhorar uma página.** Pode propor a correção de erros
  tipograficos ou de conteúdo simplesmente melhorar o texto
  facilmente, clicando no link "Ajude-nos a melhorar esta página!" que
  se encontra na base da página do manual em questão.


## Como é que eu faço bold, listas, etc?

Este guia for escrito em Markdown. É muito fácil e é quase como
escrever num ficheiro de texto. Um bom guia é
[este](https://www.markdowntutorial.com/).


::: tip Dica

Para coisas mais avançadas como esta caixa, por exemplo, veja
[aqui](https://v1.vuepress.vuejs.org/guide/markdown.html)

:::


## Código de Conduta

- **Respeitar a propriedade intelectual**: Por favor, submeta apenas
  textos ou images que você redigiu ou para os quais tenha permissão
  expressa para partilhar.
- **Aceito que as minhas contribuições sejam partilhadas** da mesma
  forma que foram partilhadas consigo, atravé da licença livre deste
  projeto.
- **Respeitar os outros**: Por favor, seja cívica(o). Respeito os
  outros como gosta que a(o) respeitem a si. Ataques à dignidade de
  alguém são expressamente proibidos.


## Lista de Contribuidores

Varias(os) decidiram permanecer anónimos mas os outros encontram-se na
lista abaixo (esta lista é periodicamente ):

- Francisco Core
- Vasilis Ververis (criou o projeto que serviu de base a este)
